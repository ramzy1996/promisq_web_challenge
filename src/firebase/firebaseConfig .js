import { initializeApp } from 'firebase/app';
import { getStorage } from 'firebase/storage';
import { getFunctions } from 'firebase/functions';

const firebaseConfig = {
    apiKey: "AIzaSyAi_ZxZzDKa-16C9bzErePkeGXASRKdGIg",
    authDomain: "promiseqwebchallange.firebaseapp.com",
    projectId: "promiseqwebchallange",
    storageBucket: "promiseqwebchallange.appspot.com",
    messagingSenderId: "442083017176",
    appId: "1:442083017176:web:4dc3a15f067776cd65f2ad"
};

const app = initializeApp(firebaseConfig);
const storage = getStorage(app);
const functions = getFunctions(app);

export { app, storage, functions };
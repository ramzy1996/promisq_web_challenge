import React, { useState } from 'react';
import { storage, functions } from '../firebase/firebaseConfig ';
import { ref, uploadBytes, getDownloadURL } from 'firebase/storage';
import { httpsCallable } from 'firebase/functions';
import Button from '@mui/material/Button';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';


const ImageUpload = ({ setImageUrl, setBoundingBoxes }) => {
    const [image, setImage] = useState(null);

    const handleImageChange = (e) => {
        if (e.target.files[0]) {
            setImage(e.target.files[0]);
        }
    };

    const handleImageUpload = async () => {
        try {
            const storageRef = ref(storage, `images/${image.name}`);
            await uploadBytes(storageRef, image);

            const imageUrl = await getDownloadURL(storageRef);
            const sendToThreatDetect = httpsCallable(functions, 'sendToThreatDetect');
            const apiResponse = await sendToThreatDetect({ imageUrl });

            setBoundingBoxes(apiResponse.data.boundingBoxes);
            setImageUrl(imageUrl);
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <div style={{ textAlign: 'center', marginTop: '20px' }}>
            <input type="file" style={{ display: 'none' }} id="upload-input" onChange={handleImageChange} />
            <label htmlFor="upload-input">
                <Button
                    variant="contained"
                    color="primary"
                    component="span"
                    startIcon={<CloudUploadIcon />}
                >
                    Upload Image
                </Button>
            </label>
            <br />
            {image && (
                <Button variant="outlined" color="secondary" onClick={handleImageUpload}>
                    Detect Objects
                </Button>
            )}
        </div>
    );
};

export default ImageUpload;

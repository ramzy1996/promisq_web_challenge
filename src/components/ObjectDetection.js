import React, { useEffect, useState } from 'react';
import Draggable from 'react-draggable';
import { Resizable } from 're-resizable';
import DeleteIcon from '@mui/icons-material/Delete';

const ObjectDetection = ({ imageUrl, boundingBoxes }) => {
    const [boxes, setBoxes] = useState(boundingBoxes);

    const handleDrag = (index, newPosition) => {
        const updatedBoxes = [...boxes];
        updatedBoxes[index].left = newPosition.x;
        updatedBoxes[index].top = newPosition.y;
        setBoxes(updatedBoxes);
    };
    useEffect(() => {
        console.log(imageUrl)
    }, [imageUrl])


    const handleResize = (index, size) => {
        const updatedBoxes = [...boxes];
        updatedBoxes[index].width = size.width;
        updatedBoxes[index].height = size.height;
        setBoxes(updatedBoxes);
    };

    const handleDeleteBox = (index) => {
        const updatedBoxes = [...boxes];
        updatedBoxes.splice(index, 1);
        setBoxes(updatedBoxes);
    };

    const renderBoundingBoxes = () => {
        return boxes.map((box, index) => (
            <Draggable key={index} onDrag={(e, data) => handleDrag(index, data)}>
                <Resizable
                    size={{ width: box.width, height: box.height }}
                    onResizeStop={(e, direction, ref, d) => handleResize(index, { width: ref.clientWidth, height: ref.clientHeight })}
                    style={{ position: 'absolute', border: '2px solid red', left: box.left, top: box.top }}
                >
                    <div>
                        <DeleteIcon
                            style={{ position: 'absolute', right: '-10px', top: '-10px', cursor: 'pointer' }}
                            onClick={() => handleDeleteBox(index)}
                        />
                    </div>
                </Resizable>
            </Draggable>
        ));
    };

    return (
        <div style={{ position: 'relative', marginTop: '20px' }}>
            <img src={imageUrl} alt="Uploaded" style={{ width: '100%' }} />
            {renderBoundingBoxes()}
        </div>
    );
};

export default ObjectDetection;

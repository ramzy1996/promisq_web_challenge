import './App.css';
import React, { useState } from 'react';
import ImageUpload from './components/ImageUpload';
import ObjectDetection from './components/ObjectDetection';

function App() {
    const [imageUrl, setImageUrl] = useState('');
  const [boundingBoxes, setBoundingBoxes] = useState([]);
  return (
    <div className="App">
      <ImageUpload setImageUrl={setImageUrl} setBoundingBoxes={setBoundingBoxes} />
      {imageUrl && <ObjectDetection imageUrl={imageUrl} boundingBoxes={boundingBoxes} />}
    </div>
  );
}

export default App;

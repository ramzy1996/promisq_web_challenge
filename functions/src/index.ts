const functions = require('firebase-functions');
const cors = require('cors')({ origin: true });
const axios = require('axios');

exports.sendToThreatDetect = functions.https.onRequest((req: any, res: any) => {
    cors(req, res, () => {
        const imageUrl = req.body.imageUrl;
        const subscriptionKey = '8abd8a9a-b22b-4680-b2dd-a2';

        axios.post('https://app.promiseq.com/api/sendToThreatDetect', { imageUrl }, {
            headers: {
                'promiseq-subscription-key': subscriptionKey,
            },
        })
            .then((response: any) => {
                res.json(response.data);
            })
            .catch((error: any) => {
                console.error(error);
                res.status(500).send('Internal Server Error');
            });
    });
});
